package com.xingfly.inventory.sdk.command;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.Min;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Data
@NoArgsConstructor
public class IncreaseInventoryCommand {
    @Min(1)
    private int increaseNumber;
}