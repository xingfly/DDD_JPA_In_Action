package com.xingfly.inventory.sdk.event;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class InventoryChangedEvent extends InventoryEvent {

    private int remains;

    public InventoryChangedEvent(String productId, int remains) {
        super((productId));
        this.remains = remains;
    }
}
