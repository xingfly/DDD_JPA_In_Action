package com.xingfly.inventory.controller;

import com.xingfly.inventory.service.InventoryAppService;
import com.xingfly.inventory.sdk.command.IncreaseInventoryCommand;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@RestController
@RequestMapping("/inventories")
public class InventoryController {

    private InventoryAppService inventoryAppService;

    public InventoryController(InventoryAppService inventoryAppService) {
        this.inventoryAppService = inventoryAppService;
    }

    @PostMapping("/{id}/increase")
    public String increaseInventory(@PathVariable("id") String inventoryId, @RequestBody @Valid IncreaseInventoryCommand command) {
        return inventoryAppService.increase(inventoryId, command);
    }
}
