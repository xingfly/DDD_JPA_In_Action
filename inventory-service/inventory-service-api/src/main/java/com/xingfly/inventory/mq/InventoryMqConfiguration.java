package com.xingfly.inventory.mq;

import com.xingfly.event.properties.RabbitProperties;
import com.xingfly.order.sdk.event.OrderCreatedEvent;
import com.xingfly.product.sdk.event.product.ProductCreatedEvent;
import com.xingfly.product.sdk.event.product.ProductNameUpdatedEvent;
import org.springframework.amqp.core.Binding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Configuration
public class InventoryMqConfiguration {

    private RabbitProperties properties;

    public InventoryMqConfiguration(RabbitProperties properties) {
        this.properties = properties;
    }


    @Bean
    public Binding bindToOrderCreated() {
        return new Binding(properties.getReceiveQ(), QUEUE, "order-publish-x", OrderCreatedEvent.class.getName(), null);
    }

    @Bean
    public Binding bindToProductCreated() {
        return new Binding(properties.getReceiveQ(), QUEUE, "product-publish-x", ProductCreatedEvent.class.getName(), null);
    }

    @Bean
    public Binding bindToProductNameUpdated() {
        return new Binding(properties.getReceiveQ(), QUEUE, "product-publish-x", ProductNameUpdatedEvent.class.getName(), null);
    }
}
