package com.xingfly.inventory.model;

import com.xingfly.jpa.common.identity.UUIDIdentity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Data
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class InventoryId extends UUIDIdentity {

    private String id;

    private InventoryId(String id) {
        this.id = id;
    }

    public InventoryId() {
        id = next();
    }

    public static InventoryId from(String inventoryId) {
        return new InventoryId(inventoryId);
    }
}
