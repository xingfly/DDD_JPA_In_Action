package com.xingfly.inventory.service;

import com.xingfly.inventory.InventoryRepository;
import com.xingfly.inventory.model.Inventory;
import com.xingfly.inventory.model.InventoryId;
import com.xingfly.inventory.sdk.command.IncreaseInventoryCommand;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Service
public class InventoryAppService {

    private InventoryRepository repository;

    public InventoryAppService(InventoryRepository inventoryRepository) {
        this.repository = inventoryRepository;
    }

    @Transactional(rollbackFor = Exception.class)
    public String increase(String inventoryId, IncreaseInventoryCommand command) {
        Inventory inventory = repository.findById(InventoryId.from(inventoryId))
                .orElseThrow(() -> new RuntimeException("Inventory Not Found!"));
        inventory.increase(command.getIncreaseNumber());
        repository.save(inventory);
        return inventoryId;
    }
}
