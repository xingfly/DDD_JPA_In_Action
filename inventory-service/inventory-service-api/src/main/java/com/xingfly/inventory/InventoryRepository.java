package com.xingfly.inventory;

import com.xingfly.inventory.model.Inventory;
import com.xingfly.inventory.model.InventoryId;
import com.xingfly.jpa.common.SoftDeleteRepository;

import java.util.Optional;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
public interface InventoryRepository extends SoftDeleteRepository<Inventory, InventoryId> {

    Optional<Inventory> findByProductId(String productId);

}
