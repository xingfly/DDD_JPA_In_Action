package com.xingfly.product.category;

import com.xingfly.jpa.common.identity.UUIDIdentity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;

/**
 * Created by SuperS on 2019/11/8.
 *
 * @author SuperS
 */
@Data
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class CategoryId extends UUIDIdentity {

    private String id;

    private CategoryId(String id) {
        this.id = id;
    }

    public CategoryId() {
        id = next();
    }

    public static CategoryId from(String categoryId) {
        return new CategoryId(categoryId);
    }
}
