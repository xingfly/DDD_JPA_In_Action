package com.xingfly.product.category;

import com.xingfly.jpa.common.BaseEntity;
import com.xingfly.product.sdk.event.category.CategoryCreatedEvent;
import com.xingfly.product.sdk.representaion.CategoryRepresentation;
import lombok.*;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import java.time.LocalDateTime;

/**
 * 产品
 * Created by SuperS on 2019/11/8.
 *
 * @author SuperS
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "t_category")
@EqualsAndHashCode(callSuper = true)
@EntityListeners(AuditingEntityListener.class)
public class Category extends BaseEntity<CategoryId> {

    private String name;

    private String description;

    private Boolean deleted;


    public static Category create(String name, String description) {
        Category category = Category.builder()
                .name(name)
                .description(description)
                .deleted(false)
                .build();
        category.setId(new CategoryId());
        category.setCreateTime(LocalDateTime.now());
        category.registerEvent(new CategoryCreatedEvent(category.getId().getId(), name, description));
        return category;
    }

    public CategoryRepresentation toRepresentation() {
        return new CategoryRepresentation(this.getId().getId(), name, description, this.getCreateTime());
    }
}
