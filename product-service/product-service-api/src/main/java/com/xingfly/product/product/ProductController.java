package com.xingfly.product.product;

import com.xingfly.product.sdk.command.CreateProductCommand;
import com.xingfly.product.sdk.command.UpdateProductNameCommand;
import com.xingfly.product.sdk.representaion.ProductWithCategoryRepresentation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by SuperS on∂ 2019/11/21.
 *
 * @author SuperS
 */
@RestController
@RequestMapping("/products")
public class ProductController {

    private ProductAppService productAppService;

    private ProductRepresentationService productRepresentationService;

    public ProductController(ProductAppService productAppService, ProductRepresentationService productRepresentationService) {
        this.productAppService = productAppService;
        this.productRepresentationService = productRepresentationService;
    }

    @PostMapping
    public ProductId createProduct(@Valid @RequestBody CreateProductCommand command) {
        return productAppService.create(command);
    }

    @PutMapping("/{id}/name")
    public ProductId updateProductName(@PathVariable("id") String id,
                                       @Valid @RequestBody UpdateProductNameCommand command) {
        return productAppService.updateProduceName(id, command);
    }

    @GetMapping("/{id}/with-category")
    public ProductWithCategoryRepresentation productWithCategory(@PathVariable("id") String id) {
        return productRepresentationService.productWithCategory(id);
    }

}
