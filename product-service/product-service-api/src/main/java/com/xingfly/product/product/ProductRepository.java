package com.xingfly.product.product;

import com.xingfly.jpa.common.SoftDeleteRepository;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
public interface ProductRepository extends SoftDeleteRepository<Product, ProductId> {
}
