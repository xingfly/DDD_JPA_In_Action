package com.xingfly.product.category;

import com.xingfly.jpa.common.SoftDeleteRepository;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
public interface CategoryRepository extends SoftDeleteRepository<Category, CategoryId> {

}
