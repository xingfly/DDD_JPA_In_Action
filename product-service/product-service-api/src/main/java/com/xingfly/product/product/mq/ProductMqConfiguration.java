package com.xingfly.product.product.mq;

import com.xingfly.event.properties.RabbitProperties;
import com.xingfly.inventory.sdk.event.InventoryChangedEvent;
import org.springframework.amqp.core.Binding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Configuration
public class ProductMqConfiguration {

    private RabbitProperties properties;

    public ProductMqConfiguration(RabbitProperties rabbitProperties) {
        this.properties = rabbitProperties;
    }


    @Bean
    public Binding bindToInventoryChanged() {
        return new Binding(properties.getReceiveQ(), QUEUE, "inventory-publish-x", InventoryChangedEvent.class.getName(), null);
    }
}
