package com.xingfly.product.category;

import com.xingfly.product.sdk.command.CreateCategoryCommand;
import com.xingfly.product.sdk.representaion.CategoryRepresentation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@RestController
@RequestMapping("/categories")
public class CategoryController {

    private CategoryAppService categoryAppService;

    public CategoryController(CategoryAppService categoryAppService) {
        this.categoryAppService = categoryAppService;
    }

    @PostMapping
    public CategoryId createCategory(@Valid @RequestBody CreateCategoryCommand command) {
        return categoryAppService.createCategory(command);
    }

    @GetMapping("/{id}")
    public CategoryRepresentation findById(@PathVariable("id") String id) {
        return categoryAppService.findById(id);
    }

}
