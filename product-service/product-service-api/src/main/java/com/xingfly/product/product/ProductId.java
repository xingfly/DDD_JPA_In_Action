package com.xingfly.product.product;

import com.xingfly.jpa.common.identity.UUIDIdentity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;

/**
 * Created by SuperS on 2019/11/8.
 *
 * @author SuperS
 */
@Data
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class ProductId extends UUIDIdentity {

    private String id;

    private ProductId(String id) {
        this.id = id;
    }

    public ProductId() {
        id = next();
    }

    public static ProductId from(String productId) {
        return new ProductId(productId);
    }
}
