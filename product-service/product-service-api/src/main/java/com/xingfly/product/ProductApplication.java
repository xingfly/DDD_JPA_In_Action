package com.xingfly.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * 产品微服务
 *
 * @author SuperS
 */
@EnableJpaAuditing
@SpringBootApplication
@EntityScan(value = "com.xingfly.product")
@EnableJpaRepositories(value = "com.xingfly.product")
public class ProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductApplication.class, args);
    }

}
