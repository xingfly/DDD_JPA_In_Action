package com.xingfly.product.sdk.representaion;

import lombok.Value;

import java.time.LocalDateTime;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Value
public class CategoryRepresentation {
    private String id;
    private String name;
    private String description;
    private LocalDateTime createTime;
}
