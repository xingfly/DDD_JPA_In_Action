package com.xingfly.product.sdk.representaion;

import lombok.Value;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Value
public class ProductWithCategoryRepresentation {
    private String productId;
    private String productName;

    private String categoryId;
    private String categoryName;
}
