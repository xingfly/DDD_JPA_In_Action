package com.xingfly.product.sdk.command;

import lombok.Value;

import javax.validation.constraints.NotBlank;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Value
public class CreateCategoryCommand {
    @NotBlank(message = "产品目录名字不能为空")
    private String name;

    @NotBlank(message = "产品目录描述不能为空")
    private String description;
}
