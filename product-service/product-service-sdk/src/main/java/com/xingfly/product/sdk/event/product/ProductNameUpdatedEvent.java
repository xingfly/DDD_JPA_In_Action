package com.xingfly.product.sdk.event.product;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProductNameUpdatedEvent extends ProductEvent {
    private String oldName;
    private String newName;

    public ProductNameUpdatedEvent(String id, String oldName, String newName) {
        super(id);
        this.oldName = oldName;
        this.newName = newName;
    }
}
