package com.xingfly.product.sdk.event.product;

import com.xingfly.event.shared.model.DomainEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ProductEvent extends DomainEvent {

    private String productId;

    public ProductEvent(String productId) {
        this.productId = productId;
    }
}
