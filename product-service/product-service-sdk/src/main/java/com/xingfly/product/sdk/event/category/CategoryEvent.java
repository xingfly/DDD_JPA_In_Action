package com.xingfly.product.sdk.event.category;

import com.xingfly.event.shared.model.DomainEvent;
import lombok.Getter;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Getter
public class CategoryEvent extends DomainEvent {
    private String categoryId;

    public CategoryEvent(String categoryId) {
        this.categoryId = categoryId;
    }
}
