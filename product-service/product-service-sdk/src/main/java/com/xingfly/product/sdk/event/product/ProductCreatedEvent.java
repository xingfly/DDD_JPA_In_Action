package com.xingfly.product.sdk.event.product;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Data
@NoArgsConstructor
public class ProductCreatedEvent extends ProductEvent {
    private String name;
    private String description;
    private BigDecimal price;
    private LocalDateTime createTime;

    public ProductCreatedEvent(String id, String name, String description, BigDecimal price, LocalDateTime createTime) {
        super(id);
        this.name = name;
        this.description = description;
        this.price = price;
        this.createTime = createTime;
    }
}
