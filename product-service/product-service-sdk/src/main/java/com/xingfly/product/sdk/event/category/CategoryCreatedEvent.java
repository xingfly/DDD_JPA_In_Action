package com.xingfly.product.sdk.event.category;

import lombok.Getter;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Getter
public class CategoryCreatedEvent extends CategoryEvent {

    private String name;

    private String description;

    public CategoryCreatedEvent(String categoryId, String name, String description) {
        super(categoryId);
        this.name = name;
        this.description = description;
    }
}
