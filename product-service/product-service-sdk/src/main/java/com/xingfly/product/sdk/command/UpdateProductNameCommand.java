package com.xingfly.product.sdk.command;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Data
@NoArgsConstructor
public class UpdateProductNameCommand {

    @NotBlank(message = "产品名字不能为空")
    private String name;

}
