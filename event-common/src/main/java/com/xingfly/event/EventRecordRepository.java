package com.xingfly.event;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by SuperS on 2019/11/22.
 *
 * @author SuperS
 */
public interface EventRecordRepository extends JpaRepository<EventRecord, String> {
    public EventRecord findEventRecordById(String id);
}
