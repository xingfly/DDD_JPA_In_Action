package com.xingfly.event;

import com.xingfly.jpa.common.SoftDeleteRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
@Repository
public interface EventRepository extends SoftDeleteRepository<Event, String> {

    List<Event> findTop100AndByDeletedOrderByCreateTime(Boolean deleted);
}
