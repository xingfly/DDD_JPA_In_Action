package com.xingfly.event;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by SuperS on 2019/11/22.
 *
 * @author SuperS
 */
@Data
@Entity
@Table(name = "t_event_record")
public class EventRecord {
    @Id
    private String id;
}
