package com.xingfly.event;

import org.springframework.amqp.rabbit.annotation.RabbitListener;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by SuperS on 2019/11/20.
 *
 * @author SuperS
 */
@Target({ElementType.TYPE, ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@RabbitListener(queues = {"#{'${rabbit.mq.receiveQ}'}"})
public @interface CustomRabbitListener {
}
