package com.xingfly.event;

import com.xingfly.event.properties.RabbitProperties;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
@Configurable
@ComponentScan(value = "com.xingfly.event")
@EnableJpaRepositories(value = "com.xingfly.event")
@EntityScan(value = "com.xingfly.event")
@EnableConfigurationProperties(RabbitProperties.class)
public class JpaEventConfiguration {

}
