package com.xingfly.event.component;

import com.xingfly.event.Event;
import com.xingfly.event.EventRepository;
import com.xingfly.event.shared.model.DomainEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by SuperS on 2019/11/19.
 *
 * @author SuperS
 */
@Component
public class RabbitDomainEventPublish {

    @Resource
    private EventRepository eventRepository;

    @Resource
    private DomainEventSender domainEventSender;

    public void publishNextBatch() {
        List<Event> events = eventRepository.findTop100AndByDeletedOrderByCreateTime(Boolean.FALSE);
        events.forEach(event -> {
            DomainEvent domainEvent = Event.toDomainEvent(event);
            domainEventSender.sendMessage(domainEvent);
            eventRepository.softDeleteById(event.getId());
        });
    }
}
