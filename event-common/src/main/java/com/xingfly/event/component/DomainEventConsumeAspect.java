package com.xingfly.event.component;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Created by SuperS on 2019/11/22.
 *
 * @author SuperS
 */
@Aspect
@Component
public class DomainEventConsumeAspect {

    private DomainEventConsumeWrapper domainEventConsumeWrapper;

    public DomainEventConsumeAspect(DomainEventConsumeWrapper domainEventConsumeWrapper) {
        this.domainEventConsumeWrapper = domainEventConsumeWrapper;
    }

    @Around("@annotation(org.springframework.amqp.rabbit.annotation.RabbitHandler) ||" +
            " @annotation(org.springframework.amqp.rabbit.annotation.RabbitListener)")
    public Object recordEvents(ProceedingJoinPoint joinPoint) throws Throwable {
        return domainEventConsumeWrapper.recordAndConsume(joinPoint);
    }
}
