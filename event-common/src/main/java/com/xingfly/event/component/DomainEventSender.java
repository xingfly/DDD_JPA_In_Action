package com.xingfly.event.component;

import com.xingfly.event.properties.RabbitProperties;
import com.xingfly.event.shared.model.DomainEvent;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.stereotype.Component;

/**
 * Created by SuperS on 2019/11/19.
 *
 * @author SuperS
 */
@Component
public class DomainEventSender {

    public DomainEventSender(RabbitTemplate rabbitTemplate,
                             MessageConverter messageConverter,
                             RabbitProperties rabbitProperties) {
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitTemplate.setMessageConverter(messageConverter);
        this.rabbitProperties = rabbitProperties;
    }

    private RabbitProperties rabbitProperties;
    private RabbitTemplate rabbitTemplate;

    public void sendMessage(DomainEvent event) {
        String exchangeName = rabbitProperties.getPublishX();
        String routingKey = event.getClass().getName();
        this.rabbitTemplate.convertAndSend(exchangeName, routingKey, event);
    }
}
