package com.xingfly.event.component;

import com.xingfly.event.Event;
import com.xingfly.event.EventRepository;
import com.xingfly.event.shared.model.DomainEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.annotation.Resource;

/**
 * 领域事件序列化
 * Created by SuperS on 2019/11/14.
 *
 * @author SuperS
 */
@Component
public class OrderEventPersistenceComponent {

    @Resource
    private EventRepository eventRepository;

    @Transactional(rollbackFor = Exception.class)
    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void process(DomainEvent event) {
        Event e = Event.create(event);
        eventRepository.save(e);
    }

}
