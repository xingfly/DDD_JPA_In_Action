package com.xingfly.event.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * Created by SuperS on 2019/11/20.
 *
 * @author SuperS
 */
@Data
@Validated
@ConfigurationProperties("rabbit.mq")
public class RabbitProperties {
    @NotBlank
    private String publishX;

    @NotBlank
    private String publishDlx;

    @NotBlank
    private String publishDlq;

    @NotBlank
    private String receiveQ;

    @NotBlank
    private String receiveDlx;

    @NotBlank
    private String receiveDlq;

    @NotBlank
    private String receiveRecoverX;


}

