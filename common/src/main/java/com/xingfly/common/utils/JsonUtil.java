package com.xingfly.common.utils;

import com.alibaba.fastjson.JSON;

/**
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
public class JsonUtil {


    public static String toJson(Object object) {
        return JSON.toJSONString(object);
    }

    public static <T> T fromJson(String json, Class<T> object) {
        return JSON.parseObject(json, object);
    }
}
