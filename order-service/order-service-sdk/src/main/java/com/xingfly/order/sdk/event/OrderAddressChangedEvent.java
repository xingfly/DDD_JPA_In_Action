package com.xingfly.order.sdk.event;

import com.xingfly.order.sdk.command.AddressCommand;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OrderAddressChangedEvent extends OrderEvent {

    private AddressCommand address;

    public OrderAddressChangedEvent(String orderId, AddressCommand address) {
        super(orderId);
        this.address = address;
    }
}
