package com.xingfly.order.sdk.event;

import com.xingfly.order.sdk.command.AddressCommand;
import com.xingfly.order.sdk.command.OrderItemCommand;
import lombok.*;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
@Data
@ToString
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OrderCreatedEvent extends OrderEvent {

    private BigDecimal price;
    private AddressCommand address;
    private List<OrderItemCommand> items;
    private LocalDateTime createTime;

    public OrderCreatedEvent(String orderId, BigDecimal price, AddressCommand address, List<OrderItemCommand> items, LocalDateTime createTime) {
        super(orderId);
        this.price = price;
        this.address = address;
        this.items = items;
        this.createTime = createTime;
    }


}
