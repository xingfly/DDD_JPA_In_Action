package com.xingfly.order.sdk.representation;

import lombok.Value;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Value
public class AddressRepresentation {
    private String addressLine1;

    private String addressLine2;

    private String city;

    private String country;

    private String zipCode;
}
