package com.xingfly.order.sdk.event;

import com.xingfly.event.shared.model.DomainEvent;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 订单事件
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OrderEvent extends DomainEvent {
    private String orderId;

    public OrderEvent(String orderId) {
        this.orderId = orderId;
    }
}
