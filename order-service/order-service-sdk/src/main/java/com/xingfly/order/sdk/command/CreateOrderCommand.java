package com.xingfly.order.sdk.command;

import lombok.Data;

import java.util.List;

/**
 * 创建订单命令
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
@Data
public class CreateOrderCommand {
    private List<OrderItemCommand> items;
    private AddressCommand address;

}
