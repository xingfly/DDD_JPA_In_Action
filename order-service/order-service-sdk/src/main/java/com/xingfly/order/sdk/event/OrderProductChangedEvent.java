package com.xingfly.order.sdk.event;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Created by SuperS on 2019/11/20.
 *
 * @author SuperS
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OrderProductChangedEvent extends OrderEvent {
    private String productId;
    private int originCount;
    private int newCount;

    public OrderProductChangedEvent(String id, String productId, Integer originCount, Integer newCount) {
        super(id);
        this.productId = productId;
        this.originCount = originCount;
        this.newCount = newCount;
    }
}
