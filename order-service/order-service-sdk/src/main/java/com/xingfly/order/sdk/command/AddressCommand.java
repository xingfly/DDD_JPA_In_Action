package com.xingfly.order.sdk.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by SuperS on 2019/11/20.
 *
 * @author SuperS
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AddressCommand {
    private String addressLine1;

    private String addressLine2;

    private String city;

    private String country;

    private String zipCode;
}
