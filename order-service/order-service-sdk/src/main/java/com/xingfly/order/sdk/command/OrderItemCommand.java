package com.xingfly.order.sdk.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * Created by SuperS on 2019/11/20.
 *
 * @author SuperS
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemCommand {
    private String productId;

    private String productName;

    private Integer count;

    private BigDecimal itemPrice;
}
