package com.xingfly.order.sdk.command;

import lombok.Data;

/**
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
@Data
public class ChangeOrderAddressCommand {
    private AddressCommand address;
}
