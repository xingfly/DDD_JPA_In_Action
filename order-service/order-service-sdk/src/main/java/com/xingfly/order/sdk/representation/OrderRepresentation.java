package com.xingfly.order.sdk.representation;

import lombok.Value;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by SuperS on 2019/11/20.
 *
 * @author SuperS
 */
@Value
public class OrderRepresentation {
    private String id;
    private List<OrderItemRepresentation> items;
    private BigDecimal totalPrice;
    private String status;
    private AddressRepresentation address;
    private LocalDateTime createTime;

}
