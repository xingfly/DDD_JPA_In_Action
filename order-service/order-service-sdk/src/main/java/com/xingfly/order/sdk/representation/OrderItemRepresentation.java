package com.xingfly.order.sdk.representation;

import lombok.Value;

import java.math.BigDecimal;

/**
 * Created by SuperS on 2019/11/21.
 *
 * @author SuperS
 */
@Value
public class OrderItemRepresentation {
    private final String productId;
    private final int count;
    private final BigDecimal itemPrice;
}
