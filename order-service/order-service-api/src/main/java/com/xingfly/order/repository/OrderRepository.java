package com.xingfly.order.repository;

import com.xingfly.jpa.common.SoftDeleteRepository;
import com.xingfly.order.model.Order;
import com.xingfly.order.model.OrderId;
import org.springframework.stereotype.Repository;

/**
 * 订单仓储
 * Created by SuperS on 2019/11/8.
 *
 * @author SuperS
 */
@Repository
public interface OrderRepository extends SoftDeleteRepository<Order, OrderId> {
}
