package com.xingfly.order.mq;

import com.xingfly.event.CustomRabbitListener;
import com.xingfly.order.sdk.event.OrderAddressChangedEvent;
import com.xingfly.order.sdk.event.OrderCreatedEvent;
import com.xingfly.order.sdk.event.OrderProductChangedEvent;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.stereotype.Component;

/**
 * Created by SuperS on 2019/11/19.
 *
 * @author SuperS
 */
@Component
@CustomRabbitListener
public class OrderEventHandler {

    @RabbitHandler
    public void process(OrderProductChangedEvent event){
        System.out.println("有订单被修改了数量:" + event);
    }

    @RabbitHandler
    public void processOrderCreatedEvent(OrderCreatedEvent event) {
        System.out.println("有新的订单被创建了:" + event);
    }

    @RabbitHandler
    public void processOrderAddressChanged(OrderAddressChangedEvent event) {
        System.out.println("有订单被修改了地址:" + event);
    }

}
