package com.xingfly.order.model;

import com.xingfly.order.sdk.command.OrderItemCommand;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.math.BigDecimal;

/**
 * 订单详情
 * Created by SuperS on 2019/11/7.
 *
 * @author SuperS
 */
@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
//@Entity
//@Table(name = "t_order_items")
public class OrderItem {

//    @Id
//    @GeneratedValue
//    private Long id;

    private String productId;

    private String productName;

    private Integer count;

    private BigDecimal itemPrice;

    public BigDecimal totalPrice() {
        return itemPrice.multiply(BigDecimal.valueOf(count));
    }

    public void updateCount(Integer count) {
        this.count = count;
    }

    public static OrderItem from(OrderItemCommand command) {
        return OrderItem.builder()
                .productId(command.getProductId())
                .count(command.getCount())
                .productName(command.getProductName())
                .itemPrice(command.getItemPrice())
                .build();
    }

    public static OrderItemCommand to(OrderItem item) {
        return OrderItemCommand.builder()
                .productId(item.getProductId())
                .count(item.getCount())
                .productName(item.getProductName())
                .itemPrice(item.getItemPrice())
                .build();
    }

}
