package com.xingfly.order.model;

import com.xingfly.order.sdk.command.AddressCommand;
import com.xingfly.order.sdk.representation.AddressRepresentation;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

/**
 * 值对象 Value Object
 * Created by SuperS on 2019/11/7.
 *
 * @author SuperS
 */
@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Address {

    private String addressLine1;

    private String addressLine2;

    private String city;

    private String country;

    private String zipCode;

    public AddressRepresentation toRepresentation() {
        return new AddressRepresentation(this.addressLine1, this.addressLine2, this.city, this.country, this.zipCode);
    }

    public static Address from(AddressCommand addressCommand) {
        return Address.builder()
                .addressLine1(addressCommand.getAddressLine1())
                .addressLine2(addressCommand.getAddressLine2())
                .city(addressCommand.getCity())
                .country(addressCommand.getCountry())
                .zipCode(addressCommand.getZipCode())
                .build();
    }

    public AddressCommand toCommand() {
        return AddressCommand.builder()
                .addressLine1(this.getAddressLine1())
                .addressLine2(this.getAddressLine2())
                .city(this.getCity())
                .country(this.getCountry())
                .zipCode(this.getZipCode())
                .build();
    }
}
