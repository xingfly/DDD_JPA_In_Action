package com.xingfly.order.controller;

import com.xingfly.order.service.OrderAppService;
import com.xingfly.order.model.OrderId;
import com.xingfly.order.sdk.command.ChangeOrderAddressCommand;
import com.xingfly.order.sdk.command.ChangeProductCountCommand;
import com.xingfly.order.sdk.command.CreateOrderCommand;
import com.xingfly.order.sdk.representation.OrderRepresentation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * Created by SuperS on 2019/11/14.
 *
 * @author SuperS
 */
@RestController
@RequestMapping("/orders")
public class OrderController {

    @Resource
    private OrderAppService orderAppService;

    @GetMapping("/{id}")
    public OrderRepresentation findById(@PathVariable String id) {
        return orderAppService.findById(id);
    }

    @PostMapping
    public OrderId create(@RequestBody CreateOrderCommand command) {
        return orderAppService.create(command);
    }

    @PutMapping("/{id}/address")
    public OrderId changeAddress(@PathVariable("id") String id,
                                 @Validated @RequestBody ChangeOrderAddressCommand command) {
        return orderAppService.changeOrderAddress(id, command);
    }

    @PutMapping("/{id}/products")
    public Boolean changeProductCount(@PathVariable("id") String id,
                                      @Validated @RequestBody ChangeProductCountCommand command) {
        return orderAppService.changeProductCount(id, command);
    }

}
