package com.xingfly.order.model;

import com.xingfly.jpa.common.identity.UUIDIdentity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Embeddable;

/**
 * Created by SuperS on 2019/11/8.
 *
 * @author SuperS
 */
@Data
@Embeddable
@EqualsAndHashCode(callSuper = true)
public class OrderId extends UUIDIdentity {

    private String id;

    private OrderId(String id) {
        this.id = id;
    }

    public OrderId() {
        id = next();
    }

    public static OrderId from(String orderId) {
        return new OrderId(orderId);
    }
}
