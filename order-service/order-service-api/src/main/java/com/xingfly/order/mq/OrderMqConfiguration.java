package com.xingfly.order.mq;

import com.xingfly.event.properties.RabbitProperties;
import com.xingfly.order.sdk.event.OrderAddressChangedEvent;
import com.xingfly.order.sdk.event.OrderCreatedEvent;
import com.xingfly.order.sdk.event.OrderProductChangedEvent;
import org.springframework.amqp.core.Binding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * Created by SuperS on 2019/11/19.
 *
 * @author SuperS
 */
@Configuration
public class OrderMqConfiguration {

    @Resource
    private RabbitProperties rabbitProperties;

    @Bean
    public Binding bindToOrderCreated() {
        return new Binding(rabbitProperties.getReceiveQ(), Binding.DestinationType.QUEUE, rabbitProperties.getPublishX(), OrderCreatedEvent.class.getName(), null);
    }

    @Bean
    public Binding bindToOrderAddressChanged() {
        return new Binding(rabbitProperties.getReceiveQ(), Binding.DestinationType.QUEUE, rabbitProperties.getPublishX(), OrderAddressChangedEvent.class.getName(), null);
    }

    @Bean
    public Binding bindToOrderProductChangeCount() {
        return new Binding(rabbitProperties.getReceiveQ(), Binding.DestinationType.QUEUE, rabbitProperties.getPublishX(), OrderProductChangedEvent.class.getName(), null);
    }

}
