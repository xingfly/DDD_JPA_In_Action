package com.xingfly.order.model;

/**
 * Created by SuperS on 2019/11/20.
 *
 * @author SuperS
 */
public enum  OrderStatus {
    CREATED,
    PAID
}
