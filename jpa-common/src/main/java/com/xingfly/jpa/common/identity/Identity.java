package com.xingfly.jpa.common.identity;

import java.io.Serializable;

/**
 * 身份标识
 * Created by SuperS on 2019/11/8.
 *
 * @author SuperS
 */
public interface Identity<T> extends Serializable {

    /**
     * ID
     *
     * @return value
     */
    T value();

    /**
     * Is Empty
     *
     * @return is Empty
     */
    default boolean isEmpty() {
        return value() == null;
    }

}
