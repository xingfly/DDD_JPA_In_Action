package com.xingfly.jpa.common.identity;

import java.util.UUID;

/**
 * Created by SuperS on 2019/11/8.
 *
 * @author SuperS
 */
public class UUIDIdentity implements RandomIdentity<String> {

    private String value;

    private UUIDIdentity(String value) {
        this.value = value;
    }

    protected UUIDIdentity() {
        this.value = next();
    }

    @Override
    public String next() {
        return UUID.randomUUID().toString();
    }

    @Override
    public String value() {
        return this.value;
    }


}
