package com.xingfly.jpa.common.identity;

/**
 * Created by SuperS on 2019/11/8.
 *
 * @author SuperS
 */
public interface RandomIdentity<T> extends Identity<T> {

    /**
     * 随机一个ID
     *
     * @return id
     */
    T next();
}
