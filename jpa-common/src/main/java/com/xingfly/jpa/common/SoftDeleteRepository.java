package com.xingfly.jpa.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by SuperS on 2019/11/11.
 *
 * @author SuperS
 */
@NoRepositoryBean
public interface SoftDeleteRepository<T, ID> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {


    /**
     * 软删除
     *
     * @param id id
     */
    @Modifying
    @Query("update #{#entityName} e set e.deleted=1 where e.id = :id")
    @Transactional(rollbackFor = Exception.class)
    void softDeleteById(ID id);

}
