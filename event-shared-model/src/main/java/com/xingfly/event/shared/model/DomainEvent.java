package com.xingfly.event.shared.model;

import com.xingfly.common.utils.UuidGenerator;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 领域事件
 * Created by SuperS on 2019/11/15.
 *
 * @author SuperS
 */
@Getter
@Setter
public abstract class DomainEvent {

    String eventId = UuidGenerator.newUuid();

    LocalDateTime eventCreateTime = LocalDateTime.now();

}
